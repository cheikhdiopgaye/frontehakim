import { OffreService } from './../services/offre.service';
import { Component, OnInit } from '@angular/core'; 
import {Router} from '@angular/router';

@Component({
  selector: 'app-accueilcandidat',
  templateUrl: './accueilcandidat.page.html',
  styleUrls: ['./accueilcandidat.page.scss'],
})
export class AccueilcandidatPage implements OnInit {

  offres:any;

  constructor( private offreService: OffreService,
    private router: Router) {}

  ngOnInit() {
  this.Offrelist()
  }
    Offrelist() {

    this.offreService.getOffres().subscribe(
      offres=> {
        console.log(this.offres),
        this.offres = offres;
      },
      error => {
        console.log('Erreur : ' + error);
      }
    );
  }
  onDetails(offre: any): void  {
    console.log(offre)
    this.offreService.detailoffre = offre;
    this.router.navigate(['/detailoffre']);
  } 

}

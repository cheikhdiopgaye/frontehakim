import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AccueilcandidatPage } from './accueilcandidat.page';

const routes: Routes = [
  {
    path: '',
    component: AccueilcandidatPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AccueilcandidatPage]
})
export class AccueilcandidatPageModule {}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilcandidatPage } from './accueilcandidat.page';

describe('AccueilcandidatPage', () => {
  let component: AccueilcandidatPage;
  let fixture: ComponentFixture<AccueilcandidatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccueilcandidatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilcandidatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

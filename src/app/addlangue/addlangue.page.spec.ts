import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddlanguePage } from './addlangue.page';

describe('AddlanguePage', () => {
  let component: AddlanguePage;
  let fixture: ComponentFixture<AddlanguePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddlanguePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddlanguePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

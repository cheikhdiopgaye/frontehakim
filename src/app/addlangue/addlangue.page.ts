import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CandidatService } from './../services/candidat.service';

@Component({
  selector: 'app-addlangue',
  templateUrl: './addlangue.page.html',
  styleUrls: ['./addlangue.page.scss'],
})
export class AddlanguePage implements OnInit {
  ionicForm: FormGroup;
  isSubmitted = false;
  constructor(private navCtrl:NavController, public formBuilder: FormBuilder ,  private alertCtrl: AlertController, private candidatservice: CandidatService) { }
  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      libeller: ['', [ Validators.minLength(3)]],
      niveau: ['',  [ Validators.minLength(2)]],
    })
  }
  onReturn(){
    this.navCtrl.back();
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
   
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value);
      this.candidatservice.addLangue(this.ionicForm.value).subscribe(
        rep => {
             console.log(" Informations personnelles bien ajoutées"); 
            console.log(rep);
        },
        error => {
           console.log(error);
         
        } 
      )
    }
  }
}

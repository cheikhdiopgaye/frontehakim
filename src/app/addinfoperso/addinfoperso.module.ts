import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { AddinfopersoPage } from './addinfoperso.page';
import { ReactiveFormsModule} from '@angular/forms';


const routes: Routes = [
  {
    path: '',
    component: AddinfopersoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [AddinfopersoPage]
})
export class AddinfopersoPageModule {}

import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CandidatService } from './../services/candidat.service';

@Component({
  selector: 'app-addinfoperso',
  templateUrl: './addinfoperso.page.html',
  styleUrls: ['./addinfoperso.page.scss'],
})
export class AddinfopersoPage implements OnInit {

  ionicForm: FormGroup;
  isSubmitted = false;
  constructor(private navCtrl:NavController, public formBuilder: FormBuilder ,  private alertCtrl: AlertController, private candidatservice: CandidatService) { }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      titre: ['', [Validators.required, Validators.minLength(5)]],
      situation: ['',  [Validators.required, Validators.minLength(2)]],
      competence: ['',  [Validators.required, Validators.minLength(10)]],
      resumer: ['', [ Validators.minLength(10),Validators.minLength(10)]],
      twitter: ['', [ Validators.minLength(5)]],
      linkdIn: ['', [ Validators.minLength(5)]],
    })
  }
  onReturn(){
    this.navCtrl.back();
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  submitForm() {
    
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value);
      this.candidatservice.addInfosPerso(this.ionicForm.value).subscribe(
        rep => {
             console.log(" Informations personnelles bien ajoutées"); 
            console.log(rep);
        },
        error => {
           console.log(error);
         
        } 
      )
    }
  }

}

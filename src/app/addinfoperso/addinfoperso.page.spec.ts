import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddinfopersoPage } from './addinfoperso.page';

describe('AddinfopersoPage', () => {
  let component: AddinfopersoPage;
  let fixture: ComponentFixture<AddinfopersoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddinfopersoPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddinfopersoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CandidatService } from './../services/candidat.service';
import { Component, OnInit } from '@angular/core';
import {ToastController} from '@ionic/angular';
import {Router} from '@angular/router'; 
import { AlertController} from '@ionic/angular';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  
  candidats: Observable<any>;
 
  constructor( private candidatservice: CandidatService, 
    private router: Router,
    private alertCtrl: AlertController) {}
  ngOnInit() {
    this.CandidatList();
  }
  CandidatList() {

    this.candidatservice.getcandidats().subscribe(
      candidats => {
        console.log(this.candidats),
        this.candidats = candidats;
      },
      error => {
        console.log(error);
      }
    );
  } 
  onDetails(cand: any): void  {
    console.log(cand)
    this.candidatservice.detailcandidat= cand;
    this.router.navigate(['/detailcandidat']);
  } 
}

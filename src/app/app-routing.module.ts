import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
 import { AuthGuard } from './services/auth-guard.service'; 

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home' , canActivate: [AuthGuard]  , loadChildren: () => import('./home/home.module').then(m => m.HomePageModule) },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'addcandidat', loadChildren: './addcandidat/addcandidat.module#AddcandidatPageModule' },
  { path: 'addannonceur', loadChildren: './addannonceur/addannonceur.module#AddannonceurPageModule' },
  { path: 'choixprofil', loadChildren: './choixprofil/choixprofil.module#ChoixprofilPageModule' },
  { path: 'menu', loadChildren: './menu/menu.module#MenuPageModule' },
  { path: 'accueil', loadChildren: './accueil/accueil.module#AccueilPageModule' },
  { path: 'candidatlist'/* , canActivate:[AuthGuard] */, loadChildren: './candidatlist/candidatlist.module#CandidatlistPageModule' },
  { path: 'annonceurlist', loadChildren: './annonceurlist/annonceurlist.module#AnnonceurlistPageModule' },
  { path: 'accueilcandidat', loadChildren: './accueilcandidat/accueilcandidat.module#AccueilcandidatPageModule' },
  { path: 'forgot', loadChildren: './forgot/forgot.module#ForgotPageModule' },
  { path: 'profil', loadChildren: './profil/profil.module#ProfilPageModule' },
  { path: 'detailcandidat', loadChildren: './detailcandidat/detailcandidat.module#DetailcandidatPageModule' },
  { path: 'detailannonceur', loadChildren: './detailannonceur/detailannonceur.module#DetailannonceurPageModule' },
  { path: 'detailoffre', loadChildren: './detailoffre/detailoffre.module#DetailoffrePageModule' },
  { path: 'addoffre', loadChildren: './addoffre/addoffre.module#AddoffrePageModule' },
  { path: 'addexp', loadChildren: './addexp/addexp.module#AddexpPageModule' },
  { path: 'addformation', loadChildren: './addformation/addformation.module#AddformationPageModule' },
  { path: 'addinfoperso', loadChildren: './addinfoperso/addinfoperso.module#AddinfopersoPageModule' },
  { path: 'cv', loadChildren: './cv/cv.module#CvPageModule' },
  { path: 'addlangue', loadChildren: './addlangue/addlangue.module#AddlanguePageModule' },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddoffrePage } from './addoffre.page';

describe('AddoffrePage', () => {
  let component: AddoffrePage;
  let fixture: ComponentFixture<AddoffrePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddoffrePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddoffrePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { OffreService } from './../services/offre.service';



@Component({
  selector: 'app-addoffre',
  templateUrl: './addoffre.page.html',
  styleUrls: ['./addoffre.page.scss'],
})
export class AddoffrePage implements OnInit {
  ionicForm: FormGroup;
  isSubmitted = false;
  typecontrat = ['Cdi', 'Cdd'];
  metier;
  libel= ['Emploi', 'Stage']
  secteur;
  defaultDate = "1987-06-30";
  typecontratDefaut="Type de contrat"
  constructor(public formBuilder: FormBuilder, private offreService: OffreService) { }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      typecontrat: [this.typecontratDefaut,  [Validators.required]],
      libelle: ['',  [Validators.required]],
      libel: ['',  [Validators.required]],
      datedebut: [this.defaultDate],
      datefin: [this.defaultDate],
      libellee: ['',  [Validators.required]],
      description: ['', [Validators.minLength(10)]],
    })
   /*  this.metier = this.offreService.OffresMetier();
    this.rubrique = this.offreService.getRubrique();
    this.secteur = this.offreService.getSecteur(); */
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  getDate(e) {
    let date = new Date(e.target.value).toISOString().substring(0, 10);
    this.ionicForm.get('datedebut').setValue(date, {
      onlyself: true
    }),
    this.ionicForm.get('datefin').setValue(date, {
      onlyself: true
    })
  }
  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value)
      this.offreService.PosterOffre(this.ionicForm.value).subscribe(
        rep => {
             console.log(" Informations personnelles bien ajoutées"); 
            console.log(rep);
        },
        error => {
           console.log(error);
        } 
      )
    }
  }
}
import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  showSplash = true;
  appPages = [];
  authenticated=false;
  jwtHelper = new JwtHelperService();
   user:any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService:AuthService,
    private router:Router
  ) {
    this.initializeApp();
  }
  ngOnInit() {
      /* this.user = this.authService.userConnecte(); */
      this.user =this.authService.user;
      console.log(this.authService.user);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.login();
      setTimeout(() => this.showSplash = false, 8000);
  });
}
  private login() {
    console.log(this.authenticated);
    if(this.authenticated) {
      this.router.navigateByUrl("/home");
    } else 
        this.router.navigateByUrl("/login");
  }
 /*  tokenExpire() {
    const token=this.authService.token;
    if( this.authenticated && this.jwtHelper.isTokenExpired(token)) {
      this.authService.logout();
      this.router.navigateByUrl("/login");
    }
  } */
  isADMIN() {
    return this.authService.isAdmin();
  }
  isANNONCEUR() {
    return this.authService.isAnnonceur();
  }
  isCANDIDAT() {
    return this.authService.isCandidat();
  }
  isAuthencated(){
    return this.authService.isAuthenticated();

  }
  Deconnecte(){
    this.authService.logout();
    this.router.navigateByUrl("/login");
  }
}

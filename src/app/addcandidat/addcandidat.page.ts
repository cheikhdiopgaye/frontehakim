import { CandidatService } from './../services/candidat.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-addcandidat',
  templateUrl: './addcandidat.page.html',
  styleUrls: ['./addcandidat.page.scss'],
})
export class AddcandidatPage implements OnInit {
  sendForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private addcandidat: CandidatService, private router: Router,
    private alertCtrl: AlertController) {

      this.sendForm = this.formBuilder.group({
        prenom: '',
        nom: '',
        telephone: '',
        username: '',
        password: '',
        confirmepassword: ''
      });
     }
    
    ValidationMsg = {
      'prenom': [
        { type: 'required', message: 'Le prénom  du candidat est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un Prénom valide!' }
      ],
      'nom': [
        { type: 'required', message: 'Le nom du candidat est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un nom valide!' }
      ],
      'telephone': [
        { type: 'required', message: 'Le numéro de téléphone du candidat est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un numéro valide!' }
      ],
      'username': [
        { type: 'required', message: "L'e-mail du candidat est obligatoire" },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un e-mail valide' }
      ],
      'password': [
        { type: 'required', message: 'Le mot de passe  est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un mot passe valide!' }
      ],
      'confirmepassword': [
        { type: 'required', message: 'La confirmation  du mot de passe est obligatoire est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 6 caracteres' },
        { type: 'pattern', message: 'les mot de passe ne sont pas les memes!' }
      ]
    };
    ngOnInit() {
      this.sendForm = this.formBuilder.group({
        prenom: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/[a-z-A-Z]/)]],
        nom: ['', [Validators.required, Validators.pattern(/[a-z-A-Z]/), Validators.minLength(2)]],
        telephone: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/[0-9]/)]],
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern(/[a-z-A-Z]/)]],
        confirmepassword: ['', [Validators.required, Validators.pattern(/[a-z-A-Z]/), Validators.minLength(6)]],
        username: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
        photo:[""]
      });
    }
    
    onSubmit() {
        this.addcandidat.inscription(this.sendForm.value).subscribe(
          rep => {
            this.router.navigateByUrl("/accueilcandidat");
            this.boiteDialog("Votre inscription a bien réussi");
            console.log(rep);
        },
          error => {
            console.log('Erreur : ' + error.message);
            this.boiteDialog(error.error.message);
          }
        )
      }
      async boiteDialog(message : string) {
        const alert = await this.alertCtrl.create({
          message: message,
          buttons: [{text: 'Ok'}]
        });
        await alert.present();
      }
      onFileSelect(event) {
        if (event.target.files.length > 0) {
        const file = event.target.files[0];
         this.sendForm.get("photo").setValue(file);
       }
      }
}

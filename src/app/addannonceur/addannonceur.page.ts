import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AnnonceurService } from './../services/annonceur.service';

@Component({
  selector: 'app-addannonceur',
  templateUrl: './addannonceur.page.html',
  styleUrls: ['./addannonceur.page.scss'],
})
export class AddannonceurPage implements OnInit {
  sendForm: FormGroup;
  image: any = null;
  constructor(private formBuilder: FormBuilder, private annonceurs: AnnonceurService, private router: Router,
    private alertCtrl: AlertController,
   /*  public navCtrl: NavController, */
    /* public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public camera: Camera */

    ) {
      this.sendForm = this.formBuilder.group({
        nomentp: '',
        adressentrep: '',
        libellee: '',
        telephone: '',
        telephone1: '',
        username: '',
        password: '',
        confirmepassword: ''
      });
     }
    ValidationMsg = {
      'nomentrep': [
        { type: 'required', message: "Le nom de l'entreprise est obligatoire" },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un valide valide!' }
      ],
      'adressentrep': [
        { type: 'required', message: "L'adresse de l'entreprise est obligatoire" },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Veillez saisir une adresse valide!' }
      ],
      'libellee': [
        { type: 'required', message: "Le secteur d'activité est obligatoire" },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Les caractéres saisies ne sont pas valide!' }
      ],
      'telephone': [
        { type: 'required', message: 'Le numéro de téléphone du candidat est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un numéro valide!' }
      ],
      'telephone1': [
        { type: 'required', message: 'Le numéro de téléphone du candidat est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un numéro valide!' }
      ],
      'username': [
        { type: 'required', message: "L'e-mail du candidat est obligatoire" },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un e-mail valide' }
      ],
      'password': [
        { type: 'required', message: 'Le mot de passe  est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 2 caracteres' },
        { type: 'pattern', message: 'Rentrer un mot passe valide!' }
      ],
      'confirmepassword': [
        { type: 'required', message: 'La confirmation  du mot de passe est obligatoire est obligatoire' },
        { type: 'minlength', message: 'Vous devez remplir au moins 6 caracteres' },
        { type: 'pattern', message: 'les mot de passe ne sont pas les memes!' }
      ]
    };
    ngOnInit() {
      this.sendForm = this.formBuilder.group({
        nomentrep: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/[a-z-A-Z]/)]],
        adressentrep: ['', [Validators.required, Validators.pattern(/[a-z-A-Z]/), Validators.minLength(2)]],
        libellee: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/[a-z-A-Z]/)]],
        telephone: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/[0-9]/)]],
        telephone1: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/[0-9]/)]],
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern(/[a-z-A-Z]/)]],
        confirmepassword: ['', [Validators.required, Validators.pattern(/[a-z-A-Z]/), Validators.minLength(6)]],
        username: ['', [Validators.required, Validators.minLength(2), Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
        logo:[""]
      });
    }
    
    onSubmitA() {
        this.annonceurs.inscriptionAnnonceur(this.sendForm.value).subscribe(
          rep => {
              this.boiteDialog("Votre inscription a bien réussi");
              this.router.navigateByUrl("/home");
              console.log(rep);
          },
          error => {
            console.log('Erreur : ' + error.message);
            this.boiteDialog(error.error.message);
          }
        )
      }
      async boiteDialog(message : string) {
        const alert = await this.alertCtrl.create({
          message: message,
          buttons: [{text: 'Ok'}]
        });
        await alert.present();
      }
      /* selectImage() {
        this.actionSheetCtrl.create({
          title: 'Set Profile Picture',
          buttons: [
            {
              text: 'Take a Photo',
              handler: () => {
                this.selectImageInCamera();
              }
            },
            {
              text: 'Choose from Gallery',
              handler: () => {
                this.selectImageInGallery();
              }
            },
            {
              text: 'Cancel',
              role: 'cancel'
            }
          ]
        }).present();
      }
    
      selectImageInCamera() {
        const CAMERA_OPTIONS: CameraOptions = {
          allowEdit: true,
          quality: 100,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: this.camera.PictureSourceType.CAMERA,
          encodingType: this.camera.EncodingType.PNG,
          mediaType: this.camera.MediaType.PICTURE
        }
    
        this.camera.getPicture(CAMERA_OPTIONS).then((imageData) => {
          this.image = `data:image/jpeg;base64,${imageData}`;
        }).catch(err => console.error(err));
      }
    
      selectImageInGallery() {
        const CAMERA_OPTIONS: CameraOptions = {
          allowEdit: true,
          quality: 100,
          destinationType: this.camera.DestinationType.DATA_URL,
          sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
          encodingType: this.camera.EncodingType.PNG,
          mediaType: this.camera.MediaType.PICTURE
        }
        this.camera.getPicture(CAMERA_OPTIONS).then((imageData) => {
          this.image = `data:image/jpeg;base64,${imageData}`;
        }).catch(err => console.error(err));
      } */
    
    /*   uploadPhoto(uid) {
        let storageRef = firebase.storage().ref();
    
        let loading = this.loadingCtrl.create();
        loading.present();
    
        storageRef.child(`/profile_pictures/${uid}.png`)
          .putString(this.image, 'data_url')
          .then(imageResult => {
            this.afDb.object(`/users/${uid}/photoURL`).set(imageResult.downloadURL);
            loading.dismiss();
          }).catch(err => console.error(err));
      } */
      onFileSelect(event) {
        if (event.target.files.length > 0) {
        const file = event.target.files[0];
         this.sendForm.get("logo").setValue(file);
       }
      }
}



import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddannonceurPage } from './addannonceur.page';

describe('AddannonceurPage', () => {
  let component: AddannonceurPage;
  let fixture: ComponentFixture<AddannonceurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddannonceurPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddannonceurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CandidatService } from './../services/candidat.service';
import { Component, OnInit } from '@angular/core';
import {ToastController} from '@ionic/angular';
/* import {Router} from '@angular/router'; */
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-candidatlist',
  templateUrl: './candidatlist.page.html',
  styleUrls: ['./candidatlist.page.scss'],
})
export class CandidatlistPage implements OnInit {

 /*  candidats:any[]=[]; */

    constructor( private candidatsercice: CandidatService,
      /* private router: Router */
      public toastController: ToastController,
      private alertCtrl: AlertController) {}

ngOnInit() {
/* this.getcandidat(); */
}
 /* getcandidat() {

    this.candidatsercice.getcandidates().then(
      candidats => {
        console.log(candidats),
        this.candidats = this.candidats;
      },
      error => {
        console.log('Erreur : ' + error);
      }
    );
  } */
  async errorBox(message: string) {
    const alert = await this.alertCtrl.create({
      message: message,
      buttons: [{text: 'Ok'}]
    });
    await alert.present();
  }
  /* onDetails(transaction: string, slidingItem: IonItemSliding) {
    slidingItem.close();
    console.log(transaction)
    this.candidatsercice.detailsTransaction=this.candidats;
    this.router.navigate(['/list-details']);
  } */

}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatlistPage } from './candidatlist.page';

describe('CandidatlistPage', () => {
  let component: CandidatlistPage;
  let fixture: ComponentFixture<CandidatlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

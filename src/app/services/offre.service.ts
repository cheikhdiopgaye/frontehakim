import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Storage} from '@ionic/storage';
import {AlertController, Platform} from '@ionic/angular';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OffreService {
  detailoffre:any;
  url = 'http://localhost:8000';
  selectDetail: any;
  constructor(private http: HttpClient, private helper: JwtHelperService,
              private storage: Storage,
              private plt: Platform,
              private alertController: AlertController,
              private authService:AuthService) {
      this.plt.ready().then(() => {
          this.authService.checkToken();
      });
  }

  PosterOffre(ionic) {
      return this.http.post(this.url+'/api/security/annonce', ionic).pipe(
          catchError(e => {
              this.authService.showAlert(e.error.msg);
              throw new Error(e);
          })
      );
  }
  getOffres(): Observable<any> {
      return this.http.get<any>(this.url+'/api/lister/annonces');
  }
  OffresMetier(): Observable<any> {
    return this.http.get<any>(this.url+'/api/lister/metier');
  }
  
  getRubrique(): Observable<any> {
    return this.http.get<any>(this.url+'/api/lister/rubrique');
  }
  getSecteur(): Observable<any> {
    return this.http.get<any>(this.url+'/api/lister/secteur');
  }

  getMesOffres(): Observable<any> {
    return this.http.get<any>(this.url+'/api/security/parannonceur');
  }
  Postuler() {
    return this.http.post(this.url+'/api/security/postuler', null).pipe(
        catchError(e => {
            this.authService.showAlert(e.error.msg);
            throw new Error(e);
        })
    );
}

}

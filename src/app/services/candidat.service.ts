import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Storage} from '@ionic/storage';
import {AlertController, Platform} from '@ionic/angular';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CandidatService {
  detailcandidat: any;
  url = 'http://localhost:8000';
  constructor(private http: HttpClient, private helper: JwtHelperService,
              private storage: Storage,
              private plt: Platform,
              private alertController: AlertController,
              private authService:AuthService) {
      this.plt.ready().then(() => {
          this.authService.checkToken();
      });
  }
  inscription(send) {
    return this.http.post(this.url+'/api/inscriptionC', send).pipe(
        catchError(e => {
            this.authService.showAlert(e.error.msg);
            throw new Error(e);
        })
    );
  }
 
  getcandidats(): Observable<any> {
      return this.http.get<any>(this.url+'/api/lister/candidats');
  }

  addInfosPerso(ionic) {
    return this.http.post(this.url+'/api/security/cv', ionic).pipe(
        catchError(e => {
            this.authService.showAlert(e.error.msg);
            throw new Error(e);
        })
    );
  }
  addformation(ionic) {
    return this.http.post(this.url+'/api/security/formation', ionic).pipe(
        catchError(e => {
            this.authService.showAlert(e.error.msg);
            throw new Error(e);
        })
    );
  }

  addexperience(ionic) {
    return this.http.post(this.url+'/api/security/experience', ionic).pipe(
        catchError(e => {
            this.authService.showAlert(e.error.msg);
            throw new Error(e);
        })
    );
  }
  addLangue(ionic) {
    return this.http.post(this.url+'/api/security/langue', ionic).pipe(
        catchError(e => {
            this.authService.showAlert(e.error.msg);
            throw new Error(e);
        })
    );
  }
 
}
import { Platform, AlertController } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Storage } from '@ionic/storage';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

const TOKEN_KEY = 'access_token';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
    jwt: string;
    username: string;
    roles: Array<string>;
    url = 'http://localhost:8000';
    user = null;
    authenticationState = new BehaviorSubject(false);

    constructor(private http: HttpClient, 
      private helper: JwtHelperService, 
      private storage: Storage, 
      private plt: Platform, 
      private alertController: AlertController) {
        this.plt.ready().then(() => {
            this.checkToken();
        });
  }
  checkToken() {
    this.storage.get(TOKEN_KEY).then(token => {
        if (token) {
            const decoded = this.helper.decodeToken(token);
            const isExpired = this.helper.isTokenExpired(token);
            this.username = decoded.obj;
            this.roles = decoded.user.roles;

            if (!isExpired && (this.isCandidat() || this.isAnnonceur())) {
                this.user = decoded;
                this.authenticationState.next(true);
            } else {
                this.storage.remove(TOKEN_KEY);
            }
        }
    });
  }
  login(credentials) {
    return this.http.post(this.url+'/api/logincheck', credentials)
        .pipe(
            tap(res => {
                this.storage.set(TOKEN_KEY, res['token']);
                this.user = this.helper.decodeToken(res['token']);
                this.authenticationState.next(true);
            }),
            catchError(e => {
                this.showAlert(e.error.msg);
                throw new Error(e);
            })
        );
}
logout() {
  this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
  });
}
/* getSpecialData() {
  return this.http.get(`${this.url}/api/special`).pipe(
      catchError(e => {
          const status = e.status;
          if (status === 401) {
              this.showAlert('You are not authorized for this!');
              this.logout();
          }
          throw new Error(e);
      })
  );
} */
initParam() {
  this.jwt = undefined;
  this.username = undefined;
  this.roles = undefined;
}

 
  isCandidat() {
    return this.user.roles.indexOf('ROLE_CANDIDAT') >= 0;
  }
  isAnnonceur() {
    return this.user.roles.indexOf('ROLE_ANNONCEUR') >= 0;
  }
  isAdmin() {
    return this.user.roles.indexOf('ROLE_SUPERADMIN') >= 0;
  }

  isAuthenticated() {
    return this.authenticationState.value && (this.isCandidat() || this.isAnnonceur() || this.isAdmin());
  }
 /*  isAuthenticated() {
    return this.user.roles && (this.isCandidat() || this.isAnnonceur() || this.isAdmin());
  } */
 /*  getUserConnecte() {
    return this.http.get<any>(this.urlBack + '/api/userConnecte');
  } */
  showAlert(msg) {
    const alert = this.alertController.create({
        message: msg,
        header: 'Error',
        buttons: ['OK']
    });
    alert.then(alert => alert.present());
}
  /* userConnecte() {
     this.getUserConnecte().subscribe(
        (rep)=>{
          console.log(rep);
          this.user.id = rep.id;
          this.user.photo = rep.photo;
          this.user.prenom = rep.prenom;
          this.user.nom = rep.nom;
          this.user.telephone = rep.telephone;
          this.storage.set("user",this.user);
        },
        (error) => {
          console.log('Erreur : '+ error.error.message);
        }
      );
   }*/
  
 
}
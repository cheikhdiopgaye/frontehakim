import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.page.html',
  styleUrls: ['./cv.page.scss'],
})
export class CvPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  addinfosPerso() {
    this.router.navigateByUrl("/addinfoperso");
  }
  addexperience() {
    this.router.navigateByUrl("/addexp");
  }
  addFormations() {
    this.router.navigateByUrl("/addformation");
  }
  addLangues() {
    this.router.navigateByUrl("/addlangue");
  }

}

import { Component, OnInit } from '@angular/core';
import { CandidatService } from './../services/candidat.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-detailcandidat',
  templateUrl: './detailcandidat.page.html',
  styleUrls: ['./detailcandidat.page.scss'],
})
export class DetailcandidatPage implements OnInit {
  cand:any;
  constructor( private candidatservice: CandidatService, private navCtrl:NavController) {

    this.cand = this.candidatservice.detailcandidat;
    console.log(this.cand)
    }
  ngOnInit() {}
  onReturn(){
    this.navCtrl.back();
  }
  

}

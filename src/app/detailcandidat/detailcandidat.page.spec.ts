import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailcandidatPage } from './detailcandidat.page';

describe('DetailcandidatPage', () => {
  let component: DetailcandidatPage;
  let fixture: ComponentFixture<DetailcandidatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailcandidatPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailcandidatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { OffreService } from './../services/offre.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-annonceurlist',
  templateUrl: './annonceurlist.page.html',
  styleUrls: ['./annonceurlist.page.scss'],
})
export class AnnonceurlistPage implements OnInit {

  offres:any;

  constructor( private offreService: OffreService,
    private router: Router) {}

  ngOnInit() {
  this.MesOffrelist()
  }
    MesOffrelist() {

    this.offreService.getMesOffres().subscribe(
      offres=> {
        console.log(this.offres),
        this.offres = offres;
      },
      error => {
        console.log('Erreur : ' + error);
      }
    );
  }
  onDetails(offre: any): void  {
    console.log(offre)
    this.offreService.detailoffre = offre;
    this.router.navigate(['/detailoffre']);
  }  

}

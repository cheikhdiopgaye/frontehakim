import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnonceurlistPage } from './annonceurlist.page';

describe('AnnonceurlistPage', () => {
  let component: AnnonceurlistPage;
  let fixture: ComponentFixture<AnnonceurlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnonceurlistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnonceurlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

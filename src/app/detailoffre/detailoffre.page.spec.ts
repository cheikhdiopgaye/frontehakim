import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailoffrePage } from './detailoffre.page';

describe('DetailoffrePage', () => {
  let component: DetailoffrePage;
  let fixture: ComponentFixture<DetailoffrePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailoffrePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailoffrePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

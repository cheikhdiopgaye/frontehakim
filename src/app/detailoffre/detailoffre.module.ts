import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DetailoffrePage } from './detailoffre.page';


const routes: Routes = [
  {
    path: '',
    component: DetailoffrePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,

    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DetailoffrePage]
})
export class DetailoffrePageModule {}

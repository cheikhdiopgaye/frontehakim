import { Component, OnInit } from '@angular/core';
import { OffreService } from './../services/offre.service';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';


@Component({
  selector: 'app-detailoffre',
  templateUrl: './detailoffre.page.html',
  styleUrls: ['./detailoffre.page.scss'],
})
export class DetailoffrePage implements OnInit {
  offre:any;
  constructor(private offreService: OffreService, private navCtrl:NavController, private router: Router) {
    this.offre = this.offreService.detailoffre;
    console.log(this.offre)
  }

  ngOnInit() {}

  onReturn(){
    this.navCtrl.back();
  }
  postuler(){
    this.offreService.Postuler().subscribe(
      rep => {
        console.log(" Réussue!"); 
          console.log(rep);
      },
      error => {
          console.log(error);
        
      } 
    )
    /* this.router.navigateByUrl('/addinfoperso'); */
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { AlertController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { LoadingController } from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  providerFb: firebase.auth.FacebookAuthProvider;
  isLoading = false;
  loading: any;
  credentialsForm: FormGroup;
  isSubmitted = false;
  constructor(private router: Router,
              private authService: AuthService,
              private alertCtrl: AlertController,
              public afDB: AngularFireDatabase,
              private afAuth: AngularFireAuth,
              private fb: Facebook,
              public platform: Platform,
              public loadingController: LoadingController,
              private googlePlus: GooglePlus,
              private  formBuilder: FormBuilder
              ) {
                this.providerFb = new firebase.auth.FacebookAuthProvider();
                /* firebase.auth().onAuthStateChanged( user => {
                  if (user){
                    this.userProfile = user;
                  } else { 
                      this.userProfile = null;
                  }
                }); */
              }

ngOnInit() {
    this.authService.logout();
    this.loading =  this.loadingController.create({
      message: 'Connecting ...'
    });
    this.credentialsForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$')]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
}
  isANNONCEUR() {
    return this.authService.isAnnonceur();
  }
  isCANDIDAT() {
    return this.authService.isCandidat();
  }


  get errorControl() {
    return this.credentialsForm.controls;
  }
    onSubmit() {
      this.isSubmitted = true;
      if (!this.credentialsForm.valid) {
        console.log('Please provide all the required values!')
        return false;
      } else {
      this.isLoading = true;
      this.authService.login(this.credentialsForm.value).subscribe(
      res => {
        setTimeout(() => {}, 1500 );
        if (this.authService.isAnnonceur()) {
          this.router.navigateByUrl('/home');
        } else if (this.authService.isCandidat()) {
          this.router.navigateByUrl('/accueilcandidat');
        } else {
          this.errorLogin("Inscrivez vous ");
        }
      },
      error => {
        this.isLoading = false;
        console.log(error.error);
        if (error.error.code === 401) {
          this.errorLogin('Erreur sur le login ou le mot de passe');
        }
        else if (error.error.status === 403) {
          this.errorLogin(error.error.message);
        }
      }
    );
    }
  }
  choixProfil(){
    this.router.navigateByUrl('/choixprofil');
  }
  async errorLogin(message: string) {
    const alert = await this.alertCtrl.create({
      message: message,
      buttons: [{text: 'Ok'}]
    });
    await alert.present();
  }

  /******************Debut Facebook login********************** */
  facebookLogin() {
    if (this.platform.is('cordova')) {
      console.log('PLateforme cordova');
      this.facebookCordova();
    } else {
      console.log('PLateforme Web');
      this.facebookWeb();
    }
   }
   facebookWeb() {
    this.afAuth.auth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then((success) => {
        console.log('Info Facebook: ' + JSON.stringify(success));
		this.afDB.object('Users/' + success.user.uid).set({
              displayName: success.user.displayName,
              photoURL: success.user.photoURL,
            });
      }).catch((error) => {
        console.log('Erreur: ' + JSON.stringify(error));
      });
  }
  facebookCordova() {
    this.fb.login(['email']).then( (response) => {
        const facebookCredential = firebase.auth.FacebookAuthProvider
            .credential(response.authResponse.accessToken);
        firebase.auth().signInWithCredential(facebookCredential)
        .then((success) => {
            console.log('Info Facebook: ' + JSON.stringify(success));
            this.afDB.object('Users/' + success.user.uid).set({
              displayName: success.user.displayName,
              photoURL: success.user.photoURL
            });
        }).catch((error) => {
            console.log('Erreur: ' + JSON.stringify(error));
        });
    }).catch((error) => { console.log(error); });
  }
  /******************Debut google login********************** */
  async presentLoading(loading) {
    await loading.present();
  }

  async googleLogin() {
    let params;
    if (this.platform.is('android')) {
      params = {
        'webClientId': '124018728460-sv8cqhnnmnf0jeqbnd0apqbnu6egkhug.apps.googleusercontent.com',
        'offline': true
      }
    }
    else {
      params = {}
    }
    this.googlePlus.login(params)
      .then((response) => {
        const { idToken, accessToken } = response
        this.onLoginSuccess(idToken, accessToken);
      }).catch((error) => {
        console.log(error)
        alert('error:' + JSON.stringify(error))
      });
  }
  onLoginSuccess(accessToken, accessSecret) {
    const credential = accessSecret ? firebase.auth.GoogleAuthProvider
        .credential(accessToken, accessSecret) : firebase.auth.GoogleAuthProvider
            .credential(accessToken);
    this.afAuth.auth.signInWithCredential(credential)
      .then((response) => {
        this.router.navigate(["/profile"]);
        this.loading.dismiss();
      })

  }
  onLoginError(err) {
    console.log(err);
  }
  doForgotPassword() {
    this.router.navigateByUrl('/forgot');
  }
}
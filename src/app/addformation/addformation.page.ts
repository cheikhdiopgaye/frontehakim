import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { CandidatService } from './../services/candidat.service';

@Component({
  selector: 'app-addformation',
  templateUrl: './addformation.page.html',
  styleUrls: ['./addformation.page.scss'],
})
export class AddformationPage implements OnInit {

  ionicForm: FormGroup;
  isSubmitted = false;
  defaultDate = "1987-06-30";
  constructor(private navCtrl:NavController, public formBuilder: FormBuilder ,  private alertCtrl: AlertController, private candidatservice: CandidatService) { }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      etablissement: ['', [Validators.required, Validators.minLength(5)]],
      domaine: ['',  [Validators.required, Validators.minLength(2)]],
      diplome: ['',  [Validators.required, Validators.minLength(10)]],
      datedebut: [this.defaultDate],
      datefin: [this.defaultDate],
      description: ['', [ Validators.minLength(5)]],
    })
  }
  onReturn(){
    this.navCtrl.back();
  }
  get errorControl() {
    return this.ionicForm.controls;
  }
  getDate(e) {
    let date = new Date(e.target.value).toISOString().substring(0, 10);
    this.ionicForm.get('datedebut').setValue(date, {
      onlyself: true
    }),
    this.ionicForm.get('datefin').setValue(date, {
      onlyself: true
    })
  }
  submitForm() {
    
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      console.log('Please provide all the required values!')
      return false;
    } else {
      console.log(this.ionicForm.value);
      this.candidatservice.addformation(this.ionicForm.value).subscribe(
        rep => {
             console.log(" Informations personnelles bien ajoutées"); 
            console.log(rep);
        },
        error => {
           console.log(error);
         
        } 
      )
    }
  }
  

}

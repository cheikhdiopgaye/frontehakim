import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddformationPage } from './addformation.page';

describe('AddformationPage', () => {
  let component: AddformationPage;
  let fixture: ComponentFixture<AddformationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddformationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddformationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

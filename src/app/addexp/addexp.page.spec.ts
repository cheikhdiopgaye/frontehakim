import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddexpPage } from './addexp.page';

describe('AddexpPage', () => {
  let component: AddexpPage;
  let fixture: ComponentFixture<AddexpPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddexpPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddexpPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

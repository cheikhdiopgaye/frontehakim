import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {IonicStorageModule} from '@ionic/storage';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { AngularFireModule } from '@angular/fire'; // pour se connecter à Firebase
import { AngularFireDatabaseModule } from '@angular/fire/database'; // pour manipuler la base de données Firebase
import { AngularFireStorageModule } from '@angular/fire/storage'; // pour accéder aux fonction de stockage et de récupération des fichiers 
import { AngularFireAuthModule } from '@angular/fire/auth';
import { Storage } from '@ionic/storage';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
export const firebaseConfig = {
  apiKey: "AIzaSyDc_Iv7bzTgyvsXd6VY36M4xBY6RUxRJrk",
    authDomain: "mobileapp-30859.firebaseapp.com",
    databaseURL: "https://mobileapp-30859.firebaseio.com",
    projectId: "mobileapp-30859",
    storageBucket: "mobileapp-30859.appspot.com",
    messagingSenderId: "436063937062",
    appId: "1:436063937062:web:f77343805b183a2bd83ce7",
    measurementId: "G-MC73PPHQ1T"
};
import { Facebook } from '@ionic-native/facebook/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
export function jwtOptionsFactory(storage) {
  return {
      tokenGetter: () => {
          return storage.get('access_token');
      },
      whitelistedDomains: ['localhost:8000']
  };
}
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    IonicStorageModule.forRoot({
        name: 'SATDB', // nom bdd
        driverOrder: ['indexeddb', 'sqlite', 'websql']
    }) ,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    JwtModule.forRoot({
      jwtOptionsProvider: {
          provide: JWT_OPTIONS,
          useFactory: jwtOptionsFactory,
          deps: [Storage],
      }
  })],
  providers: [
    StatusBar,
    Facebook,
    SplashScreen,
    GooglePlus,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}


import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailannonceurPage } from './detailannonceur.page';

describe('DetailannonceurPage', () => {
  let component: DetailannonceurPage;
  let fixture: ComponentFixture<DetailannonceurPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailannonceurPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailannonceurPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoixprofilPage } from './choixprofil.page';

describe('ChoixprofilPage', () => {
  let component: ChoixprofilPage;
  let fixture: ComponentFixture<ChoixprofilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoixprofilPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoixprofilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import {Router} from '@angular/router'; 


@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.page.html',
  styleUrls: ['./forgot.page.scss'],
})
export class ForgotPage  {

  submitted: boolean= false;
  form      : FormGroup;
  user: any;
  constructor(public router: Router, public alertCtrl: AlertController) {
  this.user={firstname: "", password:"", lastname:"", birthday:"", username:""};
   this.form = new FormGroup({
      username     : new FormControl('', Validators.required)
    });
 }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Forgot');
  }

  reset(form : FormGroup){
    this.submitted = true;
    if (this.form.valid) { 
     this.router.navigateByUrl('/login');
    } else {
      this.errorForgot("Please enter your email address. ");
    }
  }

    async errorForgot(message: string) {
      const alert = await this.alertCtrl.create({
        message: message,
        buttons: [{text: 'Ok'}]
      });
      await alert.present();
    }

}

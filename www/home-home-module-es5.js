function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
  /*!***************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ion-header>\n  <ion-toolbar color=\"primary\" class=\"tetehaut\" lines=\"none\">\n      <ion-buttons slot=\"start\" >\n          <ion-menu-button></ion-menu-button>\n      </ion-buttons>\n    <ion-row>\n        <ion-col col-5> </ion-col>\n    <ion-col col-2 >\n    <img src=\"/assets/icon/logoujob.png\" class=\"imagelogo\" />\n</ion-col>\n<ion-col col-5></ion-col>\n</ion-row>\n  </ion-toolbar>\n  <ion-toolbar color=\"primary\" >\n    <ion-row>\n     <ion-col col-10>\n        <ion-searchbar ></ion-searchbar>\n    </ion-col>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content >\n        <ion-list>\n                <ion-item *ngFor=\"let cand of candidats\" color=\"light\" class=\"ion-margin-bottom\" (click)=\" onDetails(cand)\" >\n                  <ion-thumbnail slot=\"start\">\n                    <img [src]=\"cand.photo\" > \n                  </ion-thumbnail>\n                    <ion-label padding>\n                        <h2>{{cand.prenom}}</h2>\n                        <h3>{{cand.nom}}</h3>\n                        <p >{{cand.telephone}}</p>\n                    </ion-label>\n                </ion-item>\n        </ion-list>\n  \n</ion-content>\n<ion-footer>\n  <!-- <ion-row>\n    <ion-col offset-1 col-10>\n      <p>En continuant, vous accepter les conditions d'utilisation et la politique de confidentialité de Ujob</p>\n    </ion-col>\n  </ion-row> -->\n</ion-footer>";
    /***/
  },

  /***/
  "./src/app/home/home.module.ts":
  /*!*************************************!*\
    !*** ./src/app/home/home.module.ts ***!
    \*************************************/

  /*! exports provided: HomePageModule */

  /***/
  function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
      return HomePageModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/dist/fesm5.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home.page */
    "./src/app/home/home.page.ts");

    var HomePageModule = function HomePageModule() {
      _classCallCheck(this, HomePageModule);
    };

    HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([{
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
      }])],
      declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })], HomePageModule);
    /***/
  },

  /***/
  "./src/app/home/home.page.scss":
  /*!*************************************!*\
    !*** ./src/app/home/home.page.scss ***!
    \*************************************/

  /*! exports provided: default */

  /***/
  function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".welcome-card img {\n  max-height: 35vh;\n  overflow: hidden;\n}\n\nion-content {\n  --ion-background-color: #e3eeee;\n}\n\nion-toolbar {\n  --ion-background-color: #1c8c9c;\n}\n\nion-footer {\n  --ion-background: transparent!important;\n}\n\n.imagelogo {\n  width: 70%;\n  height: 70%;\n}\n\n.tetehaut {\n  height: 50%;\n}\n\n/* ion-thumbnail {\n  --background: url('/assets/img/images.png') no-repeat center center / cover,100%,100%;\n} */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2NoZWlraGdheWUvTW9iaWxlL3NyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQztFQUNDLGdCQUFBO0VBQ0EsZ0JBQUE7QUNDRjs7QURDQTtFQUNFLCtCQUFBO0FDRUY7O0FEQUE7RUFDRSwrQkFBQTtBQ0dGOztBRERBO0VBQ0UsdUNBQUE7QUNJRjs7QURGQTtFQUNFLFVBQUE7RUFDQSxXQUFBO0FDS0Y7O0FESEE7RUFDRSxXQUFBO0FDTUY7O0FESkE7O0dBQUEiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIC53ZWxjb21lLWNhcmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMzV2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn0gXG5pb24tY29udGVudHtcbiAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI2UzZWVlZTtcbn1cbmlvbi10b29sYmFye1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMWM4YzljO1xufVxuaW9uLWZvb3RlcntcbiAgLS1pb24tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQhaW1wb3J0YW50O1xufVxuLmltYWdlbG9nb3tcbiAgd2lkdGg6NzAlO1xuICBoZWlnaHQ6NzAlO1xufVxuLnRldGVoYXV0e1xuICBoZWlnaHQ6IDUwJTtcbn1cbi8qIGlvbi10aHVtYm5haWwge1xuICAtLWJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWcvaW1hZ2VzLnBuZycpIG5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIC8gY292ZXIsMTAwJSwxMDAlO1xufSAqLyIsIi53ZWxjb21lLWNhcmQgaW1nIHtcbiAgbWF4LWhlaWdodDogMzV2aDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuaW9uLWNvbnRlbnQge1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZTNlZWVlO1xufVxuXG5pb24tdG9vbGJhciB7XG4gIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxYzhjOWM7XG59XG5cbmlvbi1mb290ZXIge1xuICAtLWlvbi1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCFpbXBvcnRhbnQ7XG59XG5cbi5pbWFnZWxvZ28ge1xuICB3aWR0aDogNzAlO1xuICBoZWlnaHQ6IDcwJTtcbn1cblxuLnRldGVoYXV0IHtcbiAgaGVpZ2h0OiA1MCU7XG59XG5cbi8qIGlvbi10aHVtYm5haWwge1xuICAtLWJhY2tncm91bmQ6IHVybCgnL2Fzc2V0cy9pbWcvaW1hZ2VzLnBuZycpIG5vLXJlcGVhdCBjZW50ZXIgY2VudGVyIC8gY292ZXIsMTAwJSwxMDAlO1xufSAqLyJdfQ== */";
    /***/
  },

  /***/
  "./src/app/home/home.page.ts":
  /*!***********************************!*\
    !*** ./src/app/home/home.page.ts ***!
    \***********************************/

  /*! exports provided: HomePage */

  /***/
  function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePage", function () {
      return HomePage;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _services_candidat_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./../services/candidat.service */
    "./src/app/services/candidat.service.ts");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @ionic/angular */
    "./node_modules/@ionic/angular/__ivy_ngcc__/dist/fesm5.js");

    var HomePage = /*#__PURE__*/function () {
      function HomePage(candidatservice, router, alertCtrl) {
        _classCallCheck(this, HomePage);

        this.candidatservice = candidatservice;
        this.router = router;
        this.alertCtrl = alertCtrl;
      }

      _createClass(HomePage, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.CandidatList();
        }
      }, {
        key: "CandidatList",
        value: function CandidatList() {
          var _this = this;

          this.candidatservice.getcandidats().then(function (candidats) {
            console.log(_this.candidats), _this.candidats = candidats;
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "onDetails",
        value: function onDetails(cand) {
          console.log(cand);
          this.candidatservice.detailcandidat = cand;
          this.router.navigate(['/detailcandidat']);
        }
      }]);

      return HomePage;
    }();

    HomePage.ctorParameters = function () {
      return [{
        type: _services_candidat_service__WEBPACK_IMPORTED_MODULE_1__["CandidatService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
      }, {
        type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
      }];
    };

    HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
      selector: 'app-home',
      template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! raw-loader!./home.page.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html"))["default"],
      styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
      /*! ./home.page.scss */
      "./src/app/home/home.page.scss"))["default"]]
    }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_services_candidat_service__WEBPACK_IMPORTED_MODULE_1__["CandidatService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])], HomePage);
    /***/
  }
}]);
//# sourceMappingURL=home-home-module-es5.js.map